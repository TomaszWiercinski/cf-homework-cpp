#include <iostream>
#include <chrono>
#include <thread>
#include <atomic>

#define MULTITHREADING
#define FULL_MATRIX_COMPARISON

#define NUM_THREADS 8

std::atomic<bool> flag(false);

unsigned int bits(int16_t number) {
    unsigned int count = 0;
    while (number) {
        count++;
        number >>= 1;
    }
    return count;
}

void poly_div(int16_t a, int16_t b, int16_t* quotient, int16_t* remainder) {
    unsigned int bits_in_b = bits(b);
    *quotient = 0;

    unsigned int bitshift = 16-bits_in_b;
    while (a >= (1 << (bits_in_b-1))) {
        if (a >> (bitshift + bits_in_b - 1)) {
            a = a ^ (b << bitshift);
            *quotient += 1 << bitshift;
        }
        bitshift--;
    }

    *remainder = a;
}

int16_t polymult(int16_t a, int16_t b) {
    int32_t c = 0;
    unsigned int bitshift = 0;
    while (b > 0) {
        if (b & 1) {
            c = c ^ (a << bitshift);
        }
        bitshift++;
        b = b>>1;
    }

    // Polynomial division by 15F
    bitshift = 32-9;
    while (c >= (1 << 8)) {
        if (c >> (bitshift + 8)) {
            c = c ^ (0x15F << bitshift);
        }
        bitshift--;
    }

    return (int16_t)c;
}

int16_t mod_inv(int16_t a) {
    int16_t x = 0, lastx = 1, y = 1, lasty = 0, remainder = 0x15F, lastremainder = a, quotient;
    int16_t swap_temp;

    while (remainder) {
        swap_temp = remainder;
        poly_div(lastremainder, remainder, &quotient, &remainder);
        lastremainder = swap_temp;

        swap_temp = lastx ^ polymult(quotient, x);
        lastx = x;
        x = swap_temp;

        swap_temp = lasty ^ polymult(quotient, y);
        lasty = y;
        y = swap_temp;
    }
    poly_div(lastx, 0x15F, &quotient, &remainder);
    return remainder;
}

[[gnu::unused]]
void print_matrix(int16_t** a) {
    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            printf("%X ", a[row][col]);
        }
        printf("\n");
    }
}

void whirlpool(
        const unsigned char* w, int w_len, const int16_t* SBox, int16_t*** m, const int16_t* fill,
        const int16_t T[4][4], const int16_t k[6][4][4]) {
    auto** m_ = new int16_t*[4];
    int index = 0;

    for (int row = 0; row < 4; row++) {
        m_[row] = new int16_t[4];
        for (int col = 0; col < 4; col++) {
            if (index < w_len) {
                (*m)[row][col] = w[index];
            } else {
                (*m)[row][col] = fill[index-w_len];
            }
            index++;
        }
    }

    for (int round = 0; round < 6; round++) {

        for (int row = 0; row < 4; row++) {
            // Sbox
            for (int col = 0; col < 4; col++)
                (*m)[row][col] = SBox[(*m)[row][col]];

            // Shuffle Columns
            for (int col=0; col < 4; col++)
                m_[row][col] = (*m)[row][(row + col) % 4];
        }

        for(int i = 0; i < 4; i++)
        {
            for(int j=0; j < 4; j++)
            {
                // Add round key
                (*m)[i][j] = k[round][i][j];

                // Mix Rows
                for(int l=0; l < 4; l++)
                    (*m)[i][j] ^= polymult(T[i][l], m_[l][j]);
            }
        }

    }

    index = 0;
    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 4; col++) {
            if (index < w_len)
                (*m)[row][col] ^= w[index];
            else
                (*m)[row][col] ^= fill[index-w_len];
            index++;
        }
    }

    for (int row = 0; row < 4; row++) {
        delete[] (m_[row]);
    }
    delete[] m_;
}

int16_t Sfunc(int16_t x) {
    return polymult(0x99, mod_inv(x)) ^ 0xD5;
}

void precalculate_SBox(int16_t* SBox[]) {
    for (int i = 0; i < 256; i++)
        (*SBox)[i] = Sfunc(i);
}

bool compare_matrices(int16_t a[4][4], int16_t** b) {
    for (int row = 0; row < 4; row++)
        for (int col = 0; col < 4; col++)
            if (a[row][col] != b[row][col])
                return false;
    return true;
}

bool partial_compare_matrices(int16_t a[4][4], int16_t** b) {
    for (int row = 0; row < 4; row++)
        if (a[row][0] != b[row][0])
            return false;
    return true;
}

bool brute_force_loop(
        int16_t cipher[4][4],
        unsigned char** w,
        const int16_t* SBox,
        int16_t*** guess,
        int w_len,
        int depth,
        const char* alphabet,
        const int16_t* fill, const int16_t T[4][4], const int16_t k[6][4][4]) {

    if (depth) {
        for (int ch = 0; ch < 83; ch++) {
            (*w)[w_len - depth] = alphabet[ch];
             if (brute_force_loop(cipher, w, SBox, guess, w_len, depth-1, alphabet, fill, T, k)) {
                 return true;
             }
        }
        return false;
    } else {
        whirlpool(*w, w_len, SBox, guess, fill, T, k);
#ifdef FULL_MATRIX_COMPARISON
        return compare_matrices(cipher, *guess);
#else
        return partial_compare_matrices(cipher, *guess);
#endif
    }
}

unsigned char* brute_force(int16_t cipher[4][4], int w_len) {
    auto* SBox = new int16_t[256];
    precalculate_SBox(&SBox);
    auto** guess = new int16_t*[4];
    for (int row = 0; row < 4; row++)
        guess[row] = new int16_t[4];
    const char alphabet[83] = {
            'q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m',
            'Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M',
            '1','2','3','4','5','6','7','8','9','0','!','@','#','$','%','^','&','*','-','_','=','+','(','[','{','<',
            ')',']','}','>', '\''
    };
    const int16_t fill[16] = {0xB3, 0xC5, 0x44, 0x97, 0x42, 0x70, 0x9D, 0x88, 0x1B, 0x6A, 0xCE, 0x10, 0x13, 0xA8, 0x5F, 0x57};
    const int16_t T[4][4] = {{1, 3, 5, 2}, {2, 1, 3, 5}, {5, 2, 1, 3}, {3, 5, 2, 1}};
    const int16_t k[6][4][4] = {
            {{0xD4, 0xBB, 0x95, 0x90}, {0x60, 0x60, 0x60, 0x60}, {0x60, 0x60, 0x60, 0x60}, {0x60, 0x60, 0x60, 0x60}},
            {{0xD, 0xC, 0x66, 0x3A}, {0xFC, 0x60, 0xCA, 0x77}, {0x0, 0x29, 0x77, 0xA3}, {0xD8, 0xA, 0xF5, 0xB9}},
            {{0xCF, 0xF0, 0x71, 0x8D}, {0x50, 0xF6, 0x87, 0x7B}, {0x20, 0xB8, 0x50, 0x5F}, {0x2D, 0x9, 0x7D, 0x3C}},
            {{0xF7, 0x26, 0x33, 0x73}, {0xED, 0x3B, 0x1B, 0x36}, {0x89, 0x84, 0xE6, 0x20}, {0xA4, 0xDA, 0x66, 0x17}},
            {{0xF8, 0xB6, 0xAD, 0x56}, {0xA5, 0x4E, 0x7D, 0xF2}, {0xC8, 0xCB, 0x1D, 0x76}, {0x3C, 0x9C, 0x5B, 0x4D}},
            {{0x1, 0xBF, 0x72, 0xB1}, {0x51, 0x20, 0xE1, 0xC4}, {0x80, 0x55, 0xEB, 0x7E}, {0xBE, 0x14, 0x5C, 0xB8}},
    };

    auto* w = new unsigned char[w_len];
    for (char ch : alphabet) {
        w[0] = ch;
        printf("Checking words starting with \"%c\"\n", ch);
        if (brute_force_loop(cipher, &w, SBox, &guess, w_len, w_len-1, alphabet, fill, T, k)) {
            for (int i = 0; i < 4; i++)
                delete[] guess[i];
            delete[] guess;
            delete[] SBox;
            return w;
        }
    }
    for (int i = 0; i < 4; i++)
        delete[] guess[i];
    delete[] guess;
    delete[] SBox;
    return nullptr;
}

void brute_force_multithread(int16_t cipher[4][4], int w_len, int num_threads) {
    auto* thread_list = new std::thread[num_threads];
    int chunk_size=83/num_threads, start, end;

    auto exec_lam = [](int start, int end, int w_len, int16_t cipher[4][4]) {
        auto* SBox = new int16_t[256];
        precalculate_SBox(&SBox);
        auto** guess = new int16_t*[4];
        for (int row = 0; row < 4; row++)
            guess[row] = new int16_t[4];
        auto* w = new unsigned char[w_len];
        const char alphabet[83] = {
                'q','w','e','r','t','y','u','i','o','p','a','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m',
                'Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M',
                '1','2','3','4','5','6','7','8','9','0','!','@','#','$','%','^','&','*','-','_','=','+','(','[','{','<',
                ')',']','}','>', '\''
        };
        const int16_t fill[] = {0xB3, 0xC5, 0x44, 0x97, 0x42, 0x70, 0x9D, 0x88, 0x1B, 0x6A, 0xCE, 0x10, 0x13, 0xA8, 0x5F, 0x57};
        const int16_t T[4][4] = {{1, 3, 5, 2}, {2, 1, 3, 5}, {5, 2, 1, 3}, {3, 5, 2, 1}};
        const int16_t k[6][4][4] = {
                {{0xD4, 0xBB, 0x95, 0x90}, {0x60, 0x60, 0x60, 0x60}, {0x60, 0x60, 0x60, 0x60}, {0x60, 0x60, 0x60, 0x60}},
                {{0xD, 0xC, 0x66, 0x3A}, {0xFC, 0x60, 0xCA, 0x77}, {0x0, 0x29, 0x77, 0xA3}, {0xD8, 0xA, 0xF5, 0xB9}},
                {{0xCF, 0xF0, 0x71, 0x8D}, {0x50, 0xF6, 0x87, 0x7B}, {0x20, 0xB8, 0x50, 0x5F}, {0x2D, 0x9, 0x7D, 0x3C}},
                {{0xF7, 0x26, 0x33, 0x73}, {0xED, 0x3B, 0x1B, 0x36}, {0x89, 0x84, 0xE6, 0x20}, {0xA4, 0xDA, 0x66, 0x17}},
                {{0xF8, 0xB6, 0xAD, 0x56}, {0xA5, 0x4E, 0x7D, 0xF2}, {0xC8, 0xCB, 0x1D, 0x76}, {0x3C, 0x9C, 0x5B, 0x4D}},
                {{0x1, 0xBF, 0x72, 0xB1}, {0x51, 0x20, 0xE1, 0xC4}, {0x80, 0x55, 0xEB, 0x7E}, {0xBE, 0x14, 0x5C, 0xB8}},
        };

        for (int ch = start; ch < end; ch++) {
            /* End if another thread found a solution */
            if (flag.load())
                break;

            w[0] = alphabet[ch];
            printf("Checking words starting with \"%c\"\n", alphabet[ch]);
            if (brute_force_loop(cipher, &w, SBox, &guess, w_len, w_len-1, alphabet, fill, T, k)) {
                flag.store(true);
                for (int i = 0; i < 4; i++)
                    delete[] guess[i];
                delete[] guess;
                delete[] SBox;
                printf("Found word: \"");
                for (int i = 0; i < w_len; i++)
                    printf("%c", w[i]);
                printf("\"\n");
                delete[] w;
                break;
            }
        }
    };

    for (int thread_i = 0; thread_i < num_threads; thread_i++) {
        start = thread_i * chunk_size;
        if (thread_i == num_threads-1)
            end = 83;
        else
            end = (thread_i + 1) * chunk_size;

        printf("Starting thread #%d on range [%d, %d)...\n", thread_i, start, end);
        thread_list[thread_i] = std::thread(exec_lam, start, end, w_len, cipher);
    }


    for(int i = 0; i < num_threads; i++) {
        thread_list[i].join();
    }

    delete[] thread_list;
}

int main(int argc, char* argv[]) {
    int w_len = 4;  // Sought word length, integer between 2 and 6

    if (argc > 1) {
        sscanf_s(argv[1], "%d", &w_len);

        if (w_len > 6) {
            printf("Invalid word length argument, set to 6 instead.\n");
            w_len = 6;
        } else if (w_len < 2) {
            printf("Invalid word length argument, set to 2 instead.\n");
            w_len = 2;
        }
    }

    // Input data
    int16_t cipher[5][4][4] = {{
        /* Cipher for 2-letter word */
        {0x64, 0x26, 0xFF, 0x2F},
        {0x75, 0xC9, 0x8B, 0x3A},
        {0x05, 0xFE, 0xD9, 0xD6},
        {0x38, 0x5B, 0x1B, 0xD4}
    }, {
        /* Cipher for 3-letter word */
        {0xBF, 0x62, 0x44, 0x6B},
        {0x18, 0xC2, 0xBB, 0xF5},
        {0xF9, 0x5B, 0x65, 0x5A},
        {0xF2, 0x6C, 0xEA, 0x25}
    }, {
        /* Cipher for 4-letter word */
        {0x80, 0xDF, 0x56, 0xB2},
        {0x55, 0x9F, 0xFD, 0xAA},
        {0x91, 0x3A, 0x91, 0xED},
        {0xF3, 0x01, 0xBF, 0x1B}
    }, {
        /*Cipher for 5-letter word */
        {0xBA, 0xEF, 0x20, 0x7F},
        {0x9D, 0xE0, 0x59, 0x3E},
        {0xBC, 0x45, 0xE1, 0x55},
        {0x01, 0x09, 0x27, 0xA1}
    }, {
        /*Cipher for 6-letter word */
        {0xC8, 0xAC, 0x7E, 0x4F},
        {0x74, 0x28, 0xD4, 0x0B},
        {0x67, 0xB1, 0xB5, 0xC2},
        {0x42, 0xC5, 0xA3, 0xCB}
    }};

#ifdef MULTITHREADING
    auto start = std::chrono::steady_clock::now();
    brute_force_multithread(cipher[w_len - 2], w_len, NUM_THREADS);
    auto end = std::chrono::steady_clock::now();

    printf("\nExecution time: %lld ms\n", std::chrono::duration_cast<std::chrono::milliseconds> (end - start).count());
#else
    auto start = std::chrono::steady_clock::now();
    static unsigned char* w = brute_force(cipher[w_len - 2], w_len);
    auto end = std::chrono::steady_clock::now();

    if (w != nullptr) {
        printf("Found word: \"");
        for (int i = 0; i < w_len; i++)
            printf("%c", w[i]);
        printf("\"\n");
    } else {
        printf("Word not found!\n");
    }

    printf("\nExecution time: %lld ms\n", std::chrono::duration_cast<std::chrono::milliseconds> (end - start).count());
#endif
}
